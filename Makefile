REPO=registry.gitlab.com/docker-master/ngino_proxy
VERSION=${2:-latest}

##########################
# Build images      	 #
##########################
.PHONY: push
push: --build-images
	docker push $(REPO)

--build-images:
	docker build -t $(REPO) -f ${VERSION}/docker/Dockerfile .
	docker tag $(REPO) $(REPO):$(VERSION);