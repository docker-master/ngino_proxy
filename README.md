# ngino_proxy

Nginx proxy for php projects. Contains the default hosts configuration and additional server settings. (see below)

## Available Tags:
* 1.18 - based on nginx:1.18 included host configuration template.
* 1.21 - based on nginx:1.21.6 included:
    * host konfigurration template
    * enablec compresion (gzip)
    * improvments of safetly

## Available variables:
    * ROOT_DIR - server.root
    * PHP_BACKEND - address of PHP-FPM container
    * PHP_PORT - Port of PHP-FPM

## Usage:

```
  web:
    image: gregorwebmaster/ngino_proxy
    volumes:
      - ./app:/var/www
    environment: 
      - ROOT_DIR=/var/www
      - PHP_BACKEND=172.16.34.15   #IP or name of your PHP container
      - PHP_PORT=9000

```

